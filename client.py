import pirc
import interface
import curses
import sys
import hangedman

# Host, port and password
HOST = "irc.patwic.com"
PORT = 6667
PASS = "foobar"

irc = pirc.IRC(HOST, PORT, PASS)

# User data
NICK = "Hangman"
REALNAME = "Simko"


# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)

def hangman_run(word1, guess, faulty_guesses, correct_guesses, secret_word):
    hangedman.right_or_false(word1, guess, correct_guesses, faulty_guesses)
    faulty_print = ' '.join(faulty_guesses) + " " + "* " * (10 - len(faulty_guesses))
    secret_word = hangedman.prity_print(word1, guess, secret_word)
    return faulty_guesses, correct_guesses, secret_word, faulty_print


def guesslistener(ui):
    while True:
        msg = irc.read()
        if msg:
            usrName = "[" + ''.join(msg.split('!~')[0])[1:] + "] "
            realMSG = msg[(msg.find('PRIVMSG ') +8):]
            if realMSG.startswith(NICK + " :hangman guess "):
                guess = realMSG[(len(NICK) + len(" :hangman guess ")):]
                ui.output(usrName + "has guessed " + guess)
                irc.send("PRIVMSG " + CHAN + " :" + usrName + " has guessed " + guess)
                return guess
            else:
                ui.output(usrName + realMSG)
        else:
            msginput(ui)


def wordlistener(ui):
    while True:
        usr_msg = ui.input()
        if usr_msg:
            receiver = CHAN
            if usr_msg.startswith("hangman word"):
                return usr_msg[len("hangman word "):]
            else:
                msginput(ui, usr_msg = usr_msg)
        else:
            msglisten(ui)
                

def msglisten(ui, hangman = False):
    msg = irc.read()
    if hangman:
        guess = guesslistener(ui)
        return guess
    elif msg:
        usrName = "[" + ''.join(msg.split('!~')[0])[1:] + "] "
        realMSG = msg[(msg.find('PRIVMSG ') +8):]
        ui.output(usrName + realMSG)
        if realMSG.startswith(NICK + " :hangman start"):
            msginput(ui, True)

    
def msginput(ui, hangman = False, loop = False, usr_msg = None):
    if not usr_msg:
        usr_msg = ui.input()
    if hangman:
        if not loop:
            receiver = CHAN
            irc.send("PRIVMSG " + receiver + " :The game is on")
        word = wordlistener(ui)
        if word:
            game = True
            faulty_guesses = []
            correct_guesses = []
            secret_word = "_ " * len(word)
            faulty_print = "* * * * * * * * * * "
            while game:
                guess = msglisten(ui, True)
                usful_stuf = hangman_run(word, guess, faulty_guesses, correct_guesses, secret_word)
                irc.send("PRIVMSG " + CHAN + " :" + str(usful_stuf[2]) + " Faulty guesses: " + str(usful_stuf[3]))
                ui.output(str(usful_stuf[2]) + " Faulty guesses: " + str(usful_stuf[3]))
                faulty_guesses = usful_stuf[0]
                correct_guesses = usful_stuf[1]
                secret_word = usful_stuf[2]
                faulty_print = usful_stuf[3]
                if hangedman.is_end(word, correct_guesses, faulty_guesses):
                    if hangedman.is_win(faulty_guesses):
                        irc.send("PRIVMSG " + CHAN + " :" + "The man was saved")
                        ui.output("The man was saved")
                        game = False
                    else:
                        irc.send("PRIVMSG " + CHAN + " :" + "The man died")
                        ui.output("The man died")
                        game = False
    elif usr_msg:
        receiver = CHAN
        if usr_msg.startswith("/MSG "):
            receiver = ''.join(usr_msg.split(' ')[1])
            usr_msg = ' '.join(usr_msg.split(' ')[2:])
        elif usr_msg.startswith("/QUIT"):
            sys.exit()
        ui.set_text_color(interface.MAGENTA)
        irc.send("PRIVMSG " + receiver + " :" + usr_msg)
        ui.output(usr_msg)
        ui.set_text_color(interface.WHITE)
 
            
def main(stdscreen):

    ui = interface.TextUI(stdscreen)

    while True:
        
        msglisten(ui)
        msginput(ui)


curses.wrapper(main)
