# IRC - Internet Relay Chat

IRC är ett *protokoll* för att skicka meddelanden över Internet. En server tar
emot meddelandet och tolkar det.

För att se hur dessa meddelanden ser ut ska vi koppla upp oss mot en server
och titta på meddelanden som skickas och tas emot. Programmet `telnet` kan
ansluta till en server. Starta ett nytt terminalfönster och kör kommandot:

```
$ telnet irc.patwic.com 6667
```

Sedan måste vi registrera en användare och ansluta till en kanal. Klipp och
klistra nedanstående till terminalen (Du måste ändra *monty* på bägge
ställerna till något annat):

    PASS foobar
    NICK monty
    USER monty irc.patwic.com nil :Montezuma Xocoyotzin
    JOIN #patwic


Du är nu inne i kanalen `patwic` på servern `irc.patwic.com`. Din
`telnet`-klient tar emot och visar trafiken som skickas från servern.


    :Otto!~Otto@212.112.161.110 PRIVMSG #patwic :The only true wisdom is in knowing you know nothing.


Men vad betyder allt det här? Låt os titta närmare på IRC-protokollet.

Ett IRC-meddelande består i princip av tre delar:

Avsändare
:   `:Otto!~Otto@212.112.161.110`  
    Börjar med `:` och sedan ett `NICK`. Texten som börjar med `!~` är
    *optional*, d.v.s den kan utelämnas.

Kommando:
:   `PRIVMSG`  
    Ett av många fördefinerade kommandon med STORA bokstäver. Kommandot
    `PRIVMSG` står för *private message* och betyder att vi vill specificera
    en mottagare av meddelandet.

Argument:
:   `#patwic :The only true wisdom is in knowing you know nothing.`  
    Den data som ska skickas med ett visst kommand specificeras som argument.
    Argumenten skiljs åt med mellanslag men det sista argumentet som börjar med
    ett kolon `:` kan innehålla flera ord. I exemplet används två argument:
    kanalen `#patwic` och strängen `The only...`

# PIRC - Patwic IRC

Vi ska i denna laboration skriva en klient för IRC i Python. Klienten kommer att kunna kommunicera med riktiga servrar men även ha lite specialfunktioner som extra utmaning.

Gör en *fork* och en *clone* av övningen från [bitbucket](https://bitbucket.org/patwic/pirc).

Filen `client.py` är den ska du skriva kod för din klient i. Börja med att ändra `NICK` och `REALNAME` till något som passar dig. Undvik mellanslag, utropstecken och kolon i ditt NICK.

## 1. Koppla upp

Om du kör programmet `client.py` kommer det att koppla upp sig mot servern och 
börja lyssna efter meddelanden för att skriva ut dem när de kommer. Du kan avsluta IRC klienten med att trycka Ctrl-c.

## 2. Formattera utskrift

Istället för att bara skriva ut meddelandet som det är ska vi försöka plocka
ut avsänadre och meddelande och endast skriva ut dessa:

```text
[Otto] The only true...
```

Du kommer att behöva ändra koden i `main`-funktionen för att åstadkomma detta.
Variabeln `msg` innehåller meddelandt som kommer från servern i det format som
vi sett förut. Använd metoder i strängobjektet för att filtrera bort allt utom
avsändare och meddelande. Skriv ut dem i gränssnittet genom att anropa
`ui.output` med strängen som argument. Testkör.

## 3. Skicka ett meddelande

För att skicka meddelanden används metoden `send` som finns i `irc`-objektet,
men först måste vi läsa in det som ska skickas.

Gränssnittsobjektet `ui` har en metod `input` som innehåller det användaren
skriver i rutan längst ner. Genom att periodiskt kolla om denna innehåller
någon text an vi avgöra om ett meddelande ska skickas:

```python
usr_msg = ui.input()
if usr_msg:
    irc.send(usr_msg)
```

Testkör och skriv något! Tex:
```text
asdf
```

och trycker på enter svarar servern: 
```text
:irc.patwic.com 421 monty asdf :Unknown command
```

För att skicka meddelanden till alla andra eller en person används kommandot PRIVMSG, precis som dom meddelanden som du tar emot.

Tex:

```text
PRIVMSG #patwic :hej
```

Så skickas meddelandet "hej" till alla på kanalen patwic.

## 4. Skriv ut skickade meddelanden i gränssnittet

Anropa även `ui.output` med `usr_msg` så att du kan se vad du skickar.

## 5. Formatera skickade meddelanden

Det är jobbigt att skriva "PRIVMSG #patwic" framför alla meddelanden som ska till
kanalen. Gör så att det automatiskt läggs på framför det meddelandet som du skrev
innan det skickas till servern.

Tex om du skriver:

```text
hej
```

så skickas:

```text
PRIVMSG #patwic :hej
```


## 6. Filtrera inkommande meddelanden

Välj ett `NICK` av de som finns på kanalen och gör så att alla meddelanden från
denna avsändare Skrivs ut i en annan färg. Titta på funktionen `set_text_color` i ui
modulen. Glöm inte att återställa färgen efter du har skrivit ut det du ville ha
i en annan färg. Du kan också färgsätta det du skriver i en speciell färg.

Tillgängliga färger:

* BLUE 
* CYAN
* GREEN
* MAGENTA 
* RED
* WHITE 
* YELLOW

Tex:

    ui.set_text_color(interface.BLUE)

## 7. Skicka privat meddelande till specifik person

Hittils har alla meddelanden som du skriver skickats som `PRIVMSG` till
`#patwic`. Du kan även skicka med en annan mottagare men för detta krävs att ditt program kan känna igen ett kommando som gör detta.

Skriv kod så att alla meddelanden som börjar med `/MSG <mottagare>` skickas till denne mottagare istället för `#patwic`. 

## 8. Implementera quit

Det finns många andra kommandon i IRC-protokollet. Skriv kod som skickar ett
`QUIT`-kommando till servern om man skriver `/QUIT` i din klient. För att avsluta klienten behövs att man lägger till en import rad ibland de andra import raderna, import sys och sedan anropar sys.exit()
