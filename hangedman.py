import doctest
import os


MAX_WRONG = 10

def right_or_false(word, guess, correct_guesses, faulty_guesses):
    """
    Returns two lists, one with correct guesses and 
    one with faulty guesses
    
    Args:
    
    word                the secret word which you guess against
    guess               a singel guess which should be vatifyed
    correct_guesses     a list containing all previus correct guesses
    faulty_guesses      a list containing all previus faulty guesses
    
    >>> right_or_false("test", "t", [], [])
    (['t'], [])
    >>> right_or_false("test", "o", [], [])
    ([], ['o'])
    """
    if guess.lower() in word.lower():
        correct_guesses.append(guess.lower())
    else:
        faulty_guesses.append(guess.lower())
    return correct_guesses, faulty_guesses
#doctest.testmod()

def is_end(word, correct_guesses, faulty_guesses):
    """
    Return True if the game is over and the function
    "is_end" should be runed, otherwise it returns
    False
    
    Args:
    word                the word whitch you gais against
    correct_guesses     a list containing all correct guesses
    faulty_guesses      a list containing all faulty guesses
    
    >>> is_end("test", ['e', 't', 's'], [])
    True
    >>> is_end("test", ['e'], ['r', 'f', 'v', 'q', 'w', 'c', 'z', 'x', 'g', 'y'])
    True
    >>> is_end("test", ['e'], ['r'])
    False
    """
    if len(faulty_guesses) == MAX_WRONG:
        return True
    else:
        for e in word:
            if e not in correct_guesses:
                return False
        return True
#doctest.testmod()

def is_win(faulty_guesses):
    """
    Return False if you lose and True if you win
    
    Args:
    
    faulty_guesses      a list with all the  faulty guesses in it
    
    >>> is_win(['r', 'f', 'v', 'q', 'w', 'c', 'z', 'x', 'g', 'y'])
    False
    >>> is_win(['r'])
    True
    """
    if len(faulty_guesses) == MAX_WRONG:
        return False
    else:
        return True
#doctest.testmod()
        
def prity_print(word, guess, secret_word):
    """
    Returns a nicer vertion of all faulty and
    correct guesses
    
    Args:
    
    word            the secret word you guess against
    guess           the last guess the player made
    secret_word     the printe sensurated vertion of the word
                        
    >>> prity_print("test", "t", "_ _ _ _")
    't _ _ t'
    >>> prity_print("test", "t", "_ e _ _")
    't e _ t'
    >>> prity_print("test", "t", "_ _ _ _")
    't _ _ t'
    """
    index = []
    count = 0
    for e in word.lower():
        if e in guess.lower():
            index.append(count)
        count += 1
    times = 0
    while times != len(index):
        secret_word = secret_word[0:(index[times]*2)] + guess.lower() + secret_word[((index[times]*2) + 1):]
        times += 1
    return secret_word
#doctest.testmod()
    
def run_fuc(word):
    """
    Loop everythin until the game is over
    
    Arg:
    
    word        the secret word which you guess against
    """
    correct_guesses = []
    faulty_guesses = []
    secret_word = "_ " * len(word)
    game = True
    while game:
        guess = input("Type your guess ")
        right_or_false(word, guess, correct_guesses, faulty_guesses)
        secret_word = prity_print(word, guess, secret_word)
        faulty_print = ' '.join(faulty_guesses) + " " + "* " * (10 - len(faulty_guesses))
        print(secret_word + " Faulty guesses: " + faulty_print)
        if is_end(word, correct_guesses, faulty_guesses):
            if is_win(faulty_guesses):
                print("You guessed the word")
            else:
                print("The man wass hanged and the word was: %s"%(word.lower()))
            game = False
        
        
if __name__ == '__main__':
    word = input("Whrite you word here ")
    os.system('clear')
    run_fuc(word)
