import hangedman
import doctest

def hangman_run(word1, guess, faulty_guesses, correct_guesses, secret_word):
    """
    >>> hangman_run("test", "x", [], [], "_ _ _ _ ")
    (['x'], [], '_ _ _ _ ', 'x * * * * * * * * * ')
    >>> hangman_run("test", "t", [], [], "_ _ _ _ ")
    ([], ['t'], 't _ _ t ', ' * * * * * * * * * * ')
    """
    hangedman.right_or_false(word1, guess, correct_guesses, faulty_guesses)
    faulty_print = ' '.join(faulty_guesses) + " " + "* " * (10 - len(faulty_guesses))
    secret_word = hangedman.prity_print(word1, guess, secret_word)
    return faulty_guesses, correct_guesses, secret_word, faulty_print
doctest.testmod()
